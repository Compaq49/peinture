# Peinture

Peinture est un site internet présentant des peintures.

 ## Environnemt de développement

### Pré-requis

* PHP 7.4
* Composer
* Symfony CLI
* Docker
* docker-compose

### Lancer l'environnement de développement

docker-compose up -d
symfony serve -d